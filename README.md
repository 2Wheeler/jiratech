##Isos Technology: Technical Exercise

- Install Jira on Linux, using the tar.gz distribution.
- Install PostgreSQL for use with Jira.
- Document steps taken to complete the install including any issues encountered along the way and how they were overcome.
- Create a list of at least 3 steps that could improve the process and identify a tool that could be used for at least one of the next steps.
- optional:
  -   Automate some or all of the installation
  -   Load some sample data
- Screenshots
  -   Running application with some basic customizations (such as tickets, change colors etc.)
  -   System information screen showing database configuration.
-   Outline of steps taken to complete the task.
-   Any automation scripts you may have used or written.
